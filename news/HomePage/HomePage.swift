//
//  ListingPage.swift
//  news
//
//  Created by Laurent Sanchez on 16/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import Combine


struct HomePage: View {
    @ObservedObject var store: News
    @ObservedObject var categories: Categories
    
    var body: some View {
        NavigationView {
            VStack {
                HeaderView(title: "News")
                CategoriesView(store: self.store, categories: self.categories)
                ListingView(store: self.store, target: .constant("home"))
                Spacer()
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct HomePage_Previews: PreviewProvider {
    static var previews: some View {
        HomePage(store: News(), categories: Categories())
    }
}
