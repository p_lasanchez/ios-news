//
//  HostingController.swift
//  news
//
//  Created by Laurent Sanchez on 17/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class HostingController: UIHostingController<ContentView> {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
}
