//
//  SearchView.swift
//  news
//
//  Created by Laurent Sanchez on 05/12/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import Combine


struct SearchView: View {
    @ObservedObject var store: News
    @State private var query: String = ""
    
    var body: some View {
        HStack {
            TextField("Recherche", text: $query)
                .padding(20)
            
            Button(action: {
                self.store.getTrendingNews(query: self.query)
            }) {
                Image("search")
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 32, height: 32)
                    .padding(.trailing, 10)
            }
            
        }
        .background(Color.gray)
        .padding(.top, -25)
        .onAppear() {
            if self.query.count != 0 {
                self.store.getTrendingNews(query: self.query)
            }
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(store: News())
    }
}
