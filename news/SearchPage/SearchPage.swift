//
//  SearchPage.swift
//  news
//
//  Created by Laurent Sanchez on 16/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import Combine


struct SearchPage: View {
    @ObservedObject var store: News
    
    var body: some View {
        NavigationView {
            VStack {
                HeaderView(title: "Recherche")
                SearchView(store: self.store)
                ListingView(store: self.store, target: .constant("search"))
                Spacer()
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct SearchPage_Previews: PreviewProvider {
    static var previews: some View {
        SearchPage(store: News())
    }
}
