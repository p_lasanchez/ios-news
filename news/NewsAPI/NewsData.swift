//
//  NewsClass.swift
//  news
//
//  Created by Laurent Sanchez on 06/12/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import Foundation
import CoreData

class NewsData: NSManagedObject {
    static func all() -> [NewsData] {
        let request: NSFetchRequest<NewsData> = NewsData.fetchRequest()
        var data: [NewsData] = []
        
        do {
            data = try AppDelegate.viewContext.fetch(request)
        } catch {
            print("Error on read")
        }
        return data
    }
    
    static func saveItem(item: NewsStruct) {
        let data = NewsData(context: AppDelegate.viewContext)
        data.title = item.title
        data.text = item.text
        data.content = item.content
        data.source = item.source
        data.url = item.url
        data.image = item.image
        data.time = item.time
        
        do {
            try AppDelegate.viewContext.save()
        } catch {
            print("Data save Error : \(item.title)")
        }
    }
    
    static func deleteItem(item: NewsStruct) {
        let itemToDelete = self.getItemByTitle(title: item.title)
        AppDelegate.viewContext.delete(itemToDelete[0])
    }
    
    static func getItemByTitle(title: String) -> [NewsData] {
        let data = self.all()
        return data.filter { item in
            item.title == title
        }
    }
}
