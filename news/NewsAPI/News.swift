//
//  News.swift
//  news
//
//  Created by Laurent Sanchez on 15/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import Foundation
import Combine
import CoreData

class News: ObservableObject {
    @Published public var items: [NewsStruct] = []
    private var cancellable: AnyCancellable? = nil
    
    init() {}
    
    func getTrendingNews(category: String? = "", query: String? = "") {
        let API_KEY = "e62ced1d642d4069978f437bcb096d90"
        let url = "https://newsapi.org/v2/top-headlines?country=fr&q=\(self.urlDecode(item: query!))&category=\(category ?? "")&apiKey=\(API_KEY)"
        
        let urlComponents = URLComponents(string: url)
        var request = URLRequest(url: (urlComponents?.url!)!)
        request.httpMethod = "GET"
        
        cancellable = URLSession.shared.dataTaskPublisher(for: request)
            .map { $0.data }
            .decode(type: NewsModel.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        break
                    case .failure(let error):
                       print(error)
                }
            }, receiveValue: { items in
                
                self.items = items.articles.map { (item: Article) -> NewsStruct in
                    return NewsStruct(
                        title: item.title,
                        text: item.articleDescription ?? "",
                        content: item.content ?? "No Content Available",
                        source: item.source.name,
                        url: item.url ?? "",
                        image: self.formatImageUrl(image: item.urlToImage),
                        time: self.formatTime(date: item.publishedAt ?? "")
                    )
                }
                
                self.cancellable?.cancel()
            })
    }
    
    func reset() {
        items = []
    }
    
    func formatTime(date: String) -> String {
        let dateFormatter = ISO8601DateFormatter()
        let date = dateFormatter.date(from:date )!
        
        let nextDateFormatter = DateFormatter()
        nextDateFormatter.dateFormat = "hh:mm"
        return nextDateFormatter.string(from: date)
    }
    
    func formatImageUrl(image: String?) -> URL {
        let defaultImg = "https://icon-library.net/images/not-found-icon/not-found-icon-28.jpg"
        
        // encode image url because of special characters
        let nextImage = image?.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? defaultImg
        
        return NSURL(string: nextImage)! as URL
    }
    
    func urlDecode(item: String) -> String {
        return item.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? ""
    }
    
    func readFromLikes() {
        let localData = NewsData.all()
        let data = localData.map { (item: NewsData) -> NewsStruct in
            return NewsStruct(
                title: item.title ?? "",
                text: item.text ?? "",
                content: item.content ?? "",
                source: item.source ?? "",
                url: item.url ?? "",
                image: item.image ?? NSURL(string: "")! as URL,
                time: item.time ?? ""
            )
        }
        self.items = data
    }
}
