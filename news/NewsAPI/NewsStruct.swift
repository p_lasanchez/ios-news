//
//  NewsJson.swift
//  news
//
//  Created by Laurent Sanchez on 18/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import Foundation

struct NewsStruct: Identifiable, Hashable, Decodable {
    public static func == (lhs: NewsStruct, rhs: NewsStruct) -> Bool {
        return true
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(title)
    }
    
    public var id = UUID()
    var title: String
    var text: String
    var content: String
    var source: String
    var url: String
    var image: URL
    var time: String
    
    init(title: String, text: String, content: String, source: String, url: String, image: URL, time: String) {
        self.title = title
        self.text = text
        self.content = content
        self.source = source
        self.url = url
        self.image = image
        self.time = time
    }
}
