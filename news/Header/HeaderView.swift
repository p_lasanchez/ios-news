//
//  HeaderView.swift
//  news
//
//  Created by Laurent Sanchez on 13/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI

struct HeaderView: View {
    @State var title: String = "Test"
    
    var body: some View {
        HStack() {
            Text(title.uppercased())
                .foregroundColor(.white)
                .bold()
                .font(.largeTitle)
            Spacer()
        }
        .padding(20)
        .background(Color.init(UIColor(red: 50/255, green: 0/255, blue: 80/255, alpha: 1.0)))
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView()
    }
}
