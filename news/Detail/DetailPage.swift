//
//  DetailPage.swift
//  news
//
//  Created by Laurent Sanchez on 17/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import URLImage
import CoreData


extension Image {
    func formatDetail() -> some View {
        return self
            .resizable()
            .aspectRatio(contentMode: .fit)
    }
}

struct DetailPage: View {
    @Environment(\.presentationMode) var presentationMode
    @Binding var item: NewsStruct
    @State private var heartColor: Color = Color.black
    
    func toggleColor() {
        heartColor = heartColor == Color.black ? Color.red : Color.black
        if (heartColor == Color.red) {
            NewsData.saveItem(item: item)
        } else {
            NewsData.deleteItem(item: item)
        }
    }
    
    func link(url: String) {
        if let url = URL(string: url) {
            UIApplication.shared.open(url)
        }
    }
    
    func checkLikeOnLoad() {
        let current = NewsData.getItemByTitle(title: self.item.title)
        if current.count > 0 {
            heartColor = Color.red
        }
    }
    
    var body: some View {
        ScrollView {
            VStack {
                HStack() {
                    Button(
                        action: {
                            self.link(url: self.item.url)
                        }
                    ) {
                        Text(item.source)
                        .foregroundColor(Color.blue)
                        .font(.footnote)
                    }
                    
                    Spacer()
                }
                .padding(.leading, 15)
                .padding(.bottom, 20)
                
                HStack() {
                    Text(item.title)
                        .foregroundColor(Color.black)
                        .font(.largeTitle)
                        .bold()

                    Spacer()
                }
                .padding(.leading, 15)
                
                
                URLImage(
                    item.image,
                    placeholder: { _ in
                        Image("notfound").formatDetail()
                }) { proxy in
                    proxy.image.formatDetail()
                }
                
                Text(item.content)
                    .foregroundColor(Color.black)
                    .font(.body)
                    .lineSpacing(10)
                    .padding(15)
                
                Spacer()
            }
        }
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(
            leading: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image("back")
                    .foregroundColor(Color.black)
            },
            trailing: Button(action: {
                self.toggleColor()
            }) {
                Image("favorite")
                    .foregroundColor(heartColor)
            }
        )
        .padding(.top, 30)
        .onAppear() {
            self.checkLikeOnLoad()
        }
    }
}

struct DetailPage_Previews: PreviewProvider {
    static var previews: some View {
        DetailPage(item: .constant(NewsStruct(
            title: "",
            text: "",
            content: "",
            source: "",
            url: "",
            image: NSURL(string: "")! as URL,
            time: ""
        )))
    }
}
