//
//  ContentView.swift
//  news
//
//  Created by Laurent Sanchez on 04/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import Combine


struct ContentView: View {
    @ObservedObject var store: News = News()
    @ObservedObject var categories: Categories = Categories()
    
    var body: some View {
        TabView {
            HomePage(store: self.store, categories: self.categories)
            .tabItem {
                Image("list")
                Text("Home")
            }
            .tag(0)
            
            LikePage(store: self.store)
            .tabItem {
                Image("favorite")
                Text("Favoris")
            }
            .tag(1)
            
            SearchPage(store: self.store)
            .tabItem {
                Image("search")
                Text("Recherche")
            }
            .tag(2)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
