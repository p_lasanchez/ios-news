//
//  ListingImage.swift
//  news
//
//  Created by Laurent Sanchez on 25/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import URLImage

extension Image {
    func formatThumbnail() -> some View {
        return self
            .resizable()
            .scaledToFill()
            .frame(width: 130, height: 150)
            .clipped()
    }
}

struct ListingImage: View {
    @State var image: URL?
    
    var body: some View {
        URLImage(image!, placeholder: { _ in
            Image("notfound").formatThumbnail()
        }) { proxy in
            proxy.image.formatThumbnail()
        }
    }
}

struct ListingImage_Previews: PreviewProvider {
    static var previews: some View {
        ListingImage()
    }
}
