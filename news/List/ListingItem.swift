//
//  ListingView.swift
//  news
//
//  Created by Laurent Sanchez on 13/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import URLImage

struct ListingItem: View {
    @State var title: String = ""
    @State var text: String = ""
    @State var source: String = ""
    @State var time: String = ""
    @State var image: URL = nil
    
    var body: some View {
        Rectangle()
        .frame(height: 150)
        .overlay(
            HStack {
                ListingImage(image: image!)
                VStack(alignment: .leading) {
                    Text(title)
                        .font(.headline)
                        .padding(.bottom, 17)
                    Text(text)
                        .font(.subheadline)
                    Spacer()
                    HStack {
                        Text(source)
                        Spacer()
                        Text(time)
                    }
                    .font(.footnote)
                    .foregroundColor(.gray)
                }
                .padding(.leading, 0)
                .padding(.trailing, 10)
                .padding(.top, 10)
                .padding(.bottom, 10)
            }
            
            .background(Color.white)
        )
        .cornerRadius(10)
        .shadow(color: .gray, radius: 10, x: 2, y: 5)
        .padding(.bottom, 10)
        .padding(.top, 2)
        .padding(.trailing, 10)
    }
}

struct ListingItem_Previews: PreviewProvider {
    static var previews: some View {
        ListingItem()
    }
}
