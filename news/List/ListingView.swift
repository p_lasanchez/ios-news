//
//  ListingView.swift
//  news
//
//  Created by Laurent Sanchez on 16/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import Combine


struct ListingView: View {
    @ObservedObject var store: News
    @Binding var target: String
    
    private func listAction() {
        switch self.target {
            case "home":
                self.store.getTrendingNews()
                
            case "search":
                self.store.reset()
                
            case "like":
                self.store.readFromLikes()
                
            default:
                print("nothing to do")
        }
    }
    
    var body: some View {
        List(store.items, id: \.id) { item in
            NavigationLink(destination: DetailPage(item: .constant(item))) {
                ListingItem(
                    title: item.title,
                    text: item.text,
                    source: item.source,
                    time: item.time,
                    image: item.image
                )
            }
        }
        .padding(.leading, -5)
        .padding(.trailing, -30)
        .onAppear {
            UITableView.appearance().separatorStyle = .none
            
            // delay data call in order to avoid TabView sync error with @ObservedObject
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.listAction()
            }
        }
    }
}

struct ListingView_Previews: PreviewProvider {
    static var previews: some View {
        ListingView(store: News(), target: .constant(""))
    }
}
