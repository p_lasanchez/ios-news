//
//  File.swift
//  news
//
//  Created by Laurent Sanchez on 14/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import Foundation
import Combine


struct Category {
    let label: String
    let value: String
    var selected: Bool? = false
}

class Categories: ObservableObject {    
    @Published public var values: [Category] = [
        Category(label: "Tous", value: "", selected: true),
        Category(label: "Loisirs", value: "entertainment"),
        Category(label: "Business", value: "business"),
        Category(label: "Général", value: "general"),
        Category(label: "Santé", value: "health"),
        Category(label: "Sciences", value: "science"),
        Category(label: "Sports", value: "sports"),
        Category(label: "Technologies", value: "technology")
    ]
    
    func selectCategory(category: String) {
        self.values = self.values.map({ (item) -> Category in
            var next = item
            next.selected = next.value == category ? true : false
            return next
        })
    }
}
