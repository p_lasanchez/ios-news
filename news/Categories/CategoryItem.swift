//
//  ButtonView.swift
//  news
//
//  Created by Laurent Sanchez on 13/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import Combine

struct CategoryItem: View {
    @Binding var category: Category
    @ObservedObject var categories: Categories
    @ObservedObject var store: News
        
    var body: some View {
        Button(
            action: {
                self.categories.selectCategory(category: self.category.value)
                self.store.getTrendingNews(category: self.category.value, query: "")
            }
        ) {
            Text(category.label)
                .foregroundColor(.white)
                .font(.body)
                .bold()
        }
        .padding(10)
        .background(category.selected == true ? Color.red : Color.blue)
        .cornerRadius(10)
    }
}

struct CategoryItem_Previews: PreviewProvider {
    static var previews: some View {
        CategoryItem(
            category: .constant(Category(label: "", value: "")),
            categories: Categories(),
            store: News()
        )
    }
}
