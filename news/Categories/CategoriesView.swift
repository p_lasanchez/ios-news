//
//  ButtonView.swift
//  news
//
//  Created by Laurent Sanchez on 16/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI
import Combine

struct CategoriesView: View {
    @ObservedObject var store: News
    @ObservedObject var categories: Categories
    
    init(store: News, categories: Categories) {
        self.store = store
        self.categories = categories
    }
    
    var body: some View {
        ScrollView(.horizontal) {
            HStack() {
                ForEach(0 ..< self.categories.values.count) {
                    CategoryItem(
                        category: .constant(self.categories.values[$0]),
                        categories: self.categories,
                        store: self.store
                    )
                }
            }
        }
        .padding(.leading, 10)
        .padding(.trailing, 10)
        .padding(.top, -10)
    }
}

struct CategoriesView_Previews: PreviewProvider {
    static var previews: some View {
        CategoriesView(store: News(), categories: Categories())
    }
}
