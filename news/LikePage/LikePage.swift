//
//  LikePage.swift
//  news
//
//  Created by Laurent Sanchez on 16/11/2019.
//  Copyright © 2019 Laurent Sanchez. All rights reserved.
//

import SwiftUI

struct LikePage: View {
    @ObservedObject var store: News
    
    var body: some View {
        NavigationView {
            VStack {
                HeaderView(title: "Favoris")
                ListingView(store: self.store, target: .constant("like"))
                Spacer()
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct LikePage_Previews: PreviewProvider {
    static var previews: some View {
        LikePage(store: News())
    }
}
